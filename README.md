# Hello Gitlab

I've been a successful Independent Contractor working remotely since 2001. Having moved on from providing traditional web design solutions for clients, I’ve spent the last 9 years focusing on projects requiring creative solutions to complex problems. I’ve applied my diverse professional background in creative services, user experience design, software development, business, and product management to achieve client goals. I enjoy a collaborative and iterative process to solve problems and strive to implement simple solutions that delight and empower end-users.

If you are reading this you probably know how to contact me... if not, maybe you can ask the person that pointed you here.

Thanks!
